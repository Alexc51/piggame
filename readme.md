## Pig Game
### REGLES DU JEU :
* Le jeu a 2 joueurs, joue chacun aprés l'autre.
* A chaque tour, un joueur lance un dé autant de fois qu'il le souhaite. Chaque résultat est ajouté à son score courant.
* Mais, si le joueur obtient un 1, tout son score courant est perdu. Après, c'est au tour du joueur suivant.
* Le joueur peut choisir de 'Hold', ce qui signifie que son score courant est ajouté à son score GLOBAL. Après, c'est au tour du joueur suivant.
* Le premier joueur à atteindre 100 points sur le score GLOBAL remporte la partie.
// REGLES DU JEU:
// - Le jeu a 2 joueurs, joue chacun aprés l'autre.
// - A chaque tour, un joueur lance un dé autant de fois qu'il le souhaite. Chaque résultat est ajouté à son score courant.
// - MAIS, si le joueur obtient un 1, tout son score courant est perdu. Après, c'est au tour du joueur suivant.
// - Le joueur peut choisir de 'Hold', ce qui signifie que son score courant est ajouté à son score GLOBAL. Après, c'est au tour du joueur suivant.
// - Le premier joueur à atteindre 100 points sur le score GLOBAL remporte la partie.

const diceDOM = document.querySelector('.dice');
const btnNew = document.querySelector('.btn-new');
const btnRoll = document.querySelector('.btn-roll');
const btnHold = document.querySelector('.btn-hold');
const current0 = document.getElementById('current-0');
const current1 = document.getElementById('current-1');
const p0p = document.querySelector('.player-0-panel');
const p1p = document.querySelector('.player-1-panel');

let scores, roundScore, activePlayer, gamePlaying;

init();

btnHold.addEventListener("click", function () {
    if (gamePlaying) {
        // Ajouter le score courant au score GLOBAL
        scores[activePlayer] += roundScore;

        // Mettre à jour l'interface utilisateur
        document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];

        // Vérifiez si le joueur a gagné le jeu
        if (scores[activePlayer] >= 100) {
            document.querySelector('#name-' + activePlayer).textContent = 'Winner !';
            diceDOM.style.display = 'none';
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
            gamePlaying = false;
        } else {
            // Sinon joueur suivant
            nextPlayer();
        }
    }
});

function nextPlayer() {
    // Joueur suivant
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    roundScore = 0;
    current0.textContent = '0';
    current1.textContent = '0';
    p0p.classList.toggle('active');
    p1p.classList.toggle('active');
    // diceDOM.style.display = 'none';
}

btnRoll.addEventListener('click', function () {
    if (gamePlaying) {
        // 1. Random number
        let dice = Math.floor(Math.random() * 6) + 1;

        //2. Afficher le résultat
        diceDOM.style.display = 'block';
        diceDOM.src = './assets/img/dice_face_' + dice + '.jpg';


        //3. Mettre à jour le score du tour, Si le nombre obtenu n'était pas un 1
        if (1 !== dice) {
            //Ajoute le score
            roundScore += dice;
            document.querySelector('#current-' + activePlayer).textContent = roundScore;
        } else {
            // Sinon joueur suivant
            nextPlayer();
        }
    }
});

//  init new game
btnNew.addEventListener("click", init);

function init() {
    scores = [0, 0];
    activePlayer = 0;
    roundScore = 0;
    gamePlaying = true;

    diceDOM.style.display = 'none';
    current0.textContent = '0';
    current1.textContent = '0';
    p0p.classList.remove('winner');
    p1p.classList.remove('winner');
    p0p.classList.remove('active');
    p1p.classList.remove('active');
    p0p.classList.add('active');
    document.getElementById('score-0').textContent = '0';
    document.getElementById('score-1').textContent = '0';
    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';
}